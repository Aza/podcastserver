import settings from './settings.json' assert { type: 'json' };

import express from 'express';
import { readdir } from 'fs';
import { statSync } from 'fs';
import { Podcast } from 'podcast';
import {itunesRoutes} from "./routes/itunes.js"

const app = express()
const port = 8000;
const mediaFolder = './medias';

//serve the media and image files
app.use(express.static('medias'));
app.use(express.static('images'));


app.get('/', (req, res) => {
    let date = new Date()
    console.log(date.toLocaleDateString()+" - "+date.toLocaleTimeString() +" : " + "Root called by "+req.ip)
    // full url of the server
    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

    // set the image url depenbding on the type (static or http)
    // uses ./images/podcast_icon.jpg. Can be changed to an http link or any image in the ./images directory
    var image = settings.image;
    if (!image.startsWith("http")) image = fullUrl + image

    // generate the rss feed content
    var feedOptions = {
        title: settings.podcast,
        description: settings.podcastDescription,
        feedUrl: fullUrl,
        siteUrl: settings.siteUrl,
        imageUrl: settings.image,
        itunesImage: image,
        author: settings.author
    }
    const feed = new Podcast(feedOptions);


    // list the files in the ./medias directory
    readdir(mediaFolder, (err, files) => {
        console.log(date.toLocaleDateString()+" - "+date.toLocaleTimeString() +" : " + "Returned rss with "+files.length+" items")
        files.forEach(file => {

            //determine the episode date depending on the file last modified time
            try {
                const stats = statSync(mediaFolder + "/" + file)
                var date = stats.mtime
                var size = stats.size
            } catch (err) {
                console.log(err);
                // handle the error
                var date = Date()
                var size  = 0
            }

            // generate the episode content
            const item = {
                title: file,
                description: settings.descriptionPrepend + file,
                url: encodeURI(fullUrl + file),
                author: settings.author,
                date: date
            }
            if (settings.useEnclosure) {

                item.enclosure = {
                    url: encodeURI(fullUrl + file),
                    size: size
                }
            }
            feed.addItem(item);
        });

        // send the http response
        res.type('application/xml');
        res.send(feed.buildXml());
    });
})

app.use('/itunes', itunesRoutes);

app.listen(port, () => {
    console.log(`Server started on ${port}`)
})





